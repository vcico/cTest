# gcc 常用命令

>  最简单用法 `gcc test.c` 生成 a.out / a.exe

## 编译过程

1. 预处理
	> gcc -E test.c  [ -o test.i ]
2. 编译为汇编代码
	> gcc -S test.i -o test.s
3. 汇编生成目标文件
	> gcc -c test.s -o test.o 
4. 链接目标文件 生成可执行文件
	> gcc test.o -o test

## 多文件编译

> gcc test1.c test2.c -o test

>> gcc -c test1.c -o test1.o

>> gcc -c test2.c -o test2.o

>> gcc test1.o test2.o -o test

# 显示warning信息
>> gcc -c main.c -Wall  

# 指定头文件目录
>> gcc -c main.c -I headPath
