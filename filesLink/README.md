#多文件链接

被多次声明的函数/变量 有且仅有一个是定义 否则无法链接

> extern  (外部链接 准确说应该是上一个同样的链接 ) 
    
    int main(){
        extern int top;  // 声明变量 (非定义 因为 不分配内存空间)
        // 如果区块内变量不加extern 就是定义
        // 如果是方法 则跟上一个一样
    }
    ------------------
    static int f();
    extern int f(); // 这里就是 内部链接  因为上一个是static

> static  (内部链接)

    static int top = -1; // 只在该文件中可以访问
