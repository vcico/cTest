# 动态库 静态库

> 动态库 静态库区别
    
    动态库：只是指定了动态链接器和库文件 并没有做真正的链接 在运行时做动态链接
    静态库：链接器会把静态库中的目标文件(用到的内容)取出来和可执行文件链接在一起
    http://docs.linuxtone.org/ebooks/C&CPP/c/ch20s03.html
## 动态库




## 静态库
    |-- main.c
    `-- stack
        |-- is_empty.c
        |-- pop.c
        |-- push.c
        |-- stack.c
        `-- stack.h
    
    1、我们把stack.c、push.c、pop.c、is_empty.c编译成目标文件
    
    gcc -c stack/stack.c stack/push.c stack/pop.c stack/is_empty.c
    
    2、然后打包成一个静态库libstack.a

    ar rs libstack.a stack.o push.o pop.o is_empty.o
        r 表示将后面的文件列表添加到文件包，如果文件包不存在就创建它，如果文件包中已有同名文件就替换成新的。
        s 是专用于生成静态库的，表示为静态库创建索引，这个索引被链接器使用
    
    3、ranlib命令也可以为静态库创建索引

    ranlib libstack.a

    4、然后我们把libstack.a和main.c编译链接在一起

    gcc main.c -L. -lstack -Istack -o main
        -L选项告诉编译器去哪里找需要的库文件，-L.表示在当前目录找。
        -lstack告诉编译器要链接libstack库，
        -I选项告诉编译器去哪里找头文件
        
        注意 : 即使库文件就在当前目录，编译器默认也不会去找的; 编译器默认会找的目录可以用-print-search-dirs选项查看
            gcc -print-search-dirs
        优先找共享库 再找静态库；如果只链接静态库
            -static 

