
#include <stdio.h>

/**
 * #pragma 设定编译器的状态 或 指示编译器完成一些特定动作
 * #pragma 参数 
 *      参数:
 *          message  编译信息输出窗口输出相应的信息
 *          code_seg 设置程序中函数代码存放的代码段
 *          once  保证头文件被编译一次
 * ANSI标准说明了5个预定义宏
 *  __LINE__ 当前被编译代码的行号
 *  __FILE__ 当前源程序的文件名
 *  __DATE__ 当前源程序的创建日期
 *  __TIME__ 当前源程序的创建时间
 *  __STDC__ 判断编译器是否为标准C 1为符合 其他不符合
 *
 *  更多 http://c.biancheng.net/cpp/html/469.html
 */

int mian()
{
    
    return 0;
}


/*
 * #line 改变行号
 *
#line 100 "linePragma.c"
#include <stdio.h>

int main()
{
    printf("1.当前的行号:%d\n",__LINE__); // 没加#line时 5 加了 104
    return 0;
}
*
