#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20

/**
 * 打印随机数的直方图
 *
 */

int a[N];

void gen_random(int upper_bound)
{
    srand(time(NULL));
    int i;
    for(i=0;i<N;i++)
    {
        a[i] = rand() % upper_bound;
    }
}

int main()
{
    int i,j, histogram[10]={0,0,0,0,0,0,0,0,0,0};
    gen_random(10);
    for(i=0;i<N;i++){
        ++histogram[a[i]];
    }
    for(i=0;i<10;i++){
        printf("%d\t",i);
    }
    printf("\n\n");
    while(1)
    {
        j=0;
        for(i=0;i<10;i++){
            if(histogram[i]>0)
            {
                j++;
                histogram[i]--;
                printf("*\t");
            }else{
                printf(" \t");
            }
        }
        printf("\n");
        if(j < 1){
            break;
        }
    }
    return 0;
}
