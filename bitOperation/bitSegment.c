#include <stdio.h>

/**
 * 位段 ： 一种特殊的结构类型  
 * 其成员的长度是以 二进制位 为单位定义的 成员变量的类型只能是 int unsigned signed 其中之一
 * 结构 结构名
 * {
     类型 变量名1:长度;
 *   ......       
 * }
 struct status{
    unsigned sign:1;
    unsigned:0;   从另一个字节开始存放 
    int score; 从下一个字节开始 分配 int 长度的空间
 }
 
 */
 
 
