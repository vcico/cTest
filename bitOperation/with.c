#include <stdio.h>

/**
 * shift + 7 
 * & 按位或
 * 只有对应的两个二进制位均为1时 结果才为1 否则为0
 * unsigned 4 byte
 * int 4 byte
 * short 2 byte
 
    10&5 = 0
      0000 1010
    & 0000 0101
    = 0000 0000 
    
    45&38 = 36
      0010 1101
    & 0010 0110
    = 0010 0100
    
 */

int main()
{
    unsigned result;
    int a;
    int b;
    printf("please input a:");
    scanf("%d",&a);
    printf("please input b:");
    scanf("%d",&b);
    printf("a=%d,b=%d",a,b);
    result = a&b;
    printf("\na&b=%u\n",result);
    return 0;
}

