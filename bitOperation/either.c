#include <stdio.h>

/**
 * | 按位或
 * 只要对应的两个二进制位有一个为1 结果就为1
 
    10&5 = 15
      0000 1010
    & 0000 0101
    = 0000 1111 
    
    45&38 = 47
      0010 1101
    & 0010 0110
    = 0010 1111
 */

int main()
{
    unsigned result;
    int a,b;
    printf("input number a:");
    scanf("%d",&a);
    printf("input number b:");
    scanf("%d",&b);
    printf("a=%d,b=%d\n",a,b);
    result = a|b;
    printf("a|b=%u\n",result);
    return 0;
}