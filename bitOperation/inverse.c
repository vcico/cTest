#include <stdio.h>

/**
 * ~ 取反运算
 * 单目运算
    对运算数各二进制位取反 0变成1 1变成0
    printf("%d\n",~10);  = -11
    10 = 0000 1010 取反后 1111 0101
    11 = 0000 1011 补码 -11 = 1111 0101
 */

int main()
{
    printf("int d = %d\n",sizeof(int));
    printf("%d\n",~10);
    printf("int u = %u\n",sizeof(unsigned));
    return 0;
}
