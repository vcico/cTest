#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/**
 *  实现弹跳小球 
 */


void main()
{
    int i,j, x =0,y =5;

    int velocity_x = 1,velocity_y=1;
    int left=0,right=20,top=0,bottom=10;
    while(1)
    {
        x = x+velocity_x;
        y = y+velocity_y;
        system("clear");
        for(i=0;i<x;i++)
            printf("\n");
        for(j=0;j<y;j++)
            printf(" ");
        printf("O\n");
        if((x==top)||(x==bottom))
            velocity_x = -velocity_x;
        if((y==left)||(y==right))
            velocity_y = -velocity_y;
        sleep(0.2);
    }
}




// 上下弹跳
void upDown()
{
    int i,j;
    int x = 5,y=10;
    int height=10, velocity=1;

    while(1){
        x = x+velocity;
        system("clear"); 

        for(i=0;i<x;i++)
            printf("\n");
        for(j=0;j<y;j++)
            printf(" ");
        printf("O\n");
     
        /*
         * 利用 i=1 -i=-1 --i=1 的特性 控制上/下
         */
        if(x==height)
            velocity = -velocity;
        if(x == 0)
            velocity = -velocity;
        sleep(1);
    }
}
