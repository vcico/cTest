#include <stdio.h>
#include <stdlib.h>

void main()
{
    int i,j;
    int x=5,y=10;
    char input;
    while(1)
    {
        system("clear");
        for(i=0;i<x;i++)
            printf("\n");
        for(j=0;j<y;j++)
            printf(" ");
        printf("*");
        printf("\n");
        input = getchar();
        system("stty echo");
        if(input == 'a')
            y--;
        if(input == 'd')
            y++;
        if(input == 'w')
            x--;
        if(input=='s')
            x++;
        if(input=='p')
            break;
    }
}
