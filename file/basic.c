#include<stdio.h>
#include <stdlib.h>
/**
 * fopen(文件名,使用方式) 打开文件 返回FILE型指针或NULL
 *   失败原因常见的:
 *      指定路径不存在、文件名含有无效字符、以r打开一个不存在的文件
 * fclose(文件指针) 关闭文件 正常关闭 返回0 否则 EOF
 *   程序结束前应关闭所有文件 以防造成数据流失
 * fputc(字符,文件指针) 吧一个字符写到磁盘文件 返回该字符或EOF
 * fgetc(文件指针) 读出一个字符  如果遇到文件结束符返回结束标志 EOF
 * fputs(字符串,文件指针) 写入字符串 返回一个非负的值 错误则EOF
 * fgets(字符串数组名,n,文件指针) 读出n个字符(含\0) 组成的字符串
 * fprintf(文件指针,格式字符串,输出列表) fprintf(fp,"%c",88); 将88以字符形式写入文件
 * fscanf(文件指针,格式字符串,输入列表) 读出字符 格式化输出
 * fread(buffer缓冲,size字节大小,count读取次数,fp文件指针)
 * fwrite(缓冲,size字节大小,count写入次数,fp文件指针)
 *
 *
 *
 */

struct address_list
{
    char name[10];
    char adr[20];
    char tel[15];
}info[100];

void save(char *name,int n)
{
    FILE *fp;
    int i;
    if((fp=fopen(name,"wb"))==NULL)
    {
        printf("cannot open file\n");
        exit(0);
    }
    for(i=0;i<n;i++)
        if(fwrite(&info[i],sizeof(struct address_list),1,fp) != 1)
            printf("file write error\n");
    fclose(fp);
}
void show(char *name,int n)
{
    int i;
    FILE *fp;
    if((fp=fopen(name,"rb"))==NULL)
    {
        printf("show : cannot open file\n");
        exit(0);
    }
    for(i=0;i<n;i++)
    {
        fread(&info[i],sizeof(struct address_list),1,fp);
        printf("%15s%20s%20s\n",info[i].name,info[i].adr,info[i].tel);
    }
    fclose(fp);
}

int main()
{
    int i,n;
    char filename[50];
    printf("how many?\n");
    scanf("%d",&n);
    printf("please input filename \n");
    scanf("%s",filename);
    printf("please input name address telephone:\n");
    for(i=0;i<n;i++)
    {
        printf("NO%d",i+1);
        scanf("%s%s%s",info[i].name,info[i].adr,info[i].tel);
    }
    save(filename,n);
    show(filename,n);
    return 0;
}









