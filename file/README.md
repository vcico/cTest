#文件

[文件的基本操作](basic.c)
[文件的定位](offset.c)

> 系统定义的FILE型结构
    
    typedef struct
    {
        short level;
        unsigned flags;
        char fd;
        unsigned char hold;
        short bsize;
        unsigned char *buffer;
        unsigned ar *curp;
        unsigned istemp;
        short token;
    }
    FILE *fp // 定义变量不必讲结构体内容都给出
