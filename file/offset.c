#include <stdio.h>

/**
 * 文件的定位
 * fseek(文件指针,位偏移量,起始点)
 *  一般用于二进制文件 文本文件需要转换 计算位置会出现错误
 *  参数 位偏移量如果是常量 加L long类型
 *  起始点: 0 首 1 当前位置 2 尾
 * rewind(文件类型指针) 重置指针到头 
 * ftell(文件指针) 得到流式文件中的当前位置 相对于文件头的位移量 -1L表示出错
 *
 */

/*
int main()
{
    FILE *fp;
    char str[50];
    fp = fopen("text","r+");
    fseek(fp,5L,0);
    fgets(str,sizeof(str),fp);
    puts(str);
    putchar('\n');
    return 0; 
}
*/
