# gdb 常用命令

> 编译时要加上 -g 选项 生成的可执行文件才能用gdb调试
>> gcc -g main.c -o main
	
	(gdb)start  					// 开始执行程序
	(gdb)next  						// 可简写为 n; 逐条执行命令
	(gdb)(直接回车)  				// 重复上一条命令
	(gdb)step 						// 可简写为 s; 该行有函数 可以进入到函数里去跟踪执行
	(gdb)backtrace  				// 可简写为 bt ; 查看函数调用的栈帧
	(gdb)info locals  				// 可简写为 i ; 查看该函数的局部变量的值
	(gdb)frame  1   				// 简写 f  根据 bt 显示的栈帧编号 查询该函数的局部变量 
	(gdb)print sum  				// 简写 p 打印sum的值 
	(gdb)finish 					//让程序一直运行到当前函数返回为止
	(gdb)set var sum=0 				// 修改变量的值
	(gdb) p result[2]=33  			// 修改变量的值 （print 后面参数是表达式 复制和函数调用都行）
	(gdb) p printf("result[2]=%d\n", result[2])
	(gdb)help  						// 查看命令类别
	(gdb)help files 				// 查看files类别下还有那些命令 
	(gdb)list 1  					// list 可简写成 l;  从第一行开始列出源代码 一次显示10行
	(gdb)list    					// 接着上面的(从11行) 继续列出代码
	(gdb)quit						// 退出gdb调试
	(gdb)l funcName  				// 显示某个函数的源代码



>参考：http://www.cnblogs.com/hankers/archive/2012/12/07/2806836.html